# Introduction
This repo contains the dotfiles I need to share between machine and users. <br />
This instructions are written mostly for me in the future, in case i forget something. <br />
This is an important distinction becase I will use ssh to clone and not https, but this should be the only difference, so if you want to try my config, just uso https (or fork this repo so you are able to use ssh). <br />
Feel free to use and modify this config! <br />

# Dependencies
The real only dependencies are git and base-devel (or equivalent on non-arch systems) and obviously [chezmoi](https://github.com/twpayne/chezmoi), the installing helper files will take care of the other deps, at least on arch.

For the full list of required software see the installing helper files:
- [./private_dot_config/yay/get_yay.sh](https://gitlab.com/aless3/dotfiles/-/blob/main/private_dot_config/yay/get-yay.sh)
- [./private_dot_config/yay/executable_yay-install.sh](https://gitlab.com/aless3/dotfiles/-/blob/main/private_dot_config/yay/executable_yay-install.sh)
- [./private_dot_config/yay/executable_extra_install.sh](https://gitlab.com/aless3/dotfiles/-/blob/main/private_dot_config/yay/executable_extra-install.sh)

I use the [btrfs filesystem](https://wiki.archlinux.org/title/Btrfs), [alacritty](https://github.com/alacritty/alacritty) as terminal emulator and [zellij](https://github.com/zellij-org/zellij) as terminal multiplexer and usually [stumpwm](https://github.com/stumpwm/stumpwm) as window manager with [SLim](https://slim-fork.sourceforge.io/) as login manager.

I also use some non-default software instead of some basic utils, remove them from .zshrc if you don't want to use them. <br />
The installing helper files will install those, remove them or uninstall them after if you don'want them even only installed. <br />
Those packages are:
- [yay](https://github.com/Jguer/yay) as package manager (I use arch btw)
- [ripgrep](https://github.com/BurntSushi/ripgrep) instead of grep
- [bat](https://github.com/sharkdp/bat) instead of cat
- [fd](https://github.com/sharkdp/fd) instead of find
- [rsync](https://github.com/WayneD/rsync) instead of cp
- [zoxide](https://github.com/ajeetdsouza/zoxide) instead of cd

# Insallation
First install chezmoi from the distro package manager (reccomended) or build it. <br />
If gitlab doesn't yet have an ssh key that authorizes your linux installation add it on gitlab!

### Clone the repo
``` shell
chezmoi init git@gitlab.com:aless3/dotfiles.git
# then if you want you can enable auto-commit and auto-push on chezmoi
cp ~/.config/chezmoi.toml ~/.config/chezmoi/chezmoi.toml
```

### Update pacman configuration
If you are on arch and use pacman (with or without helpers) this pacman.conf enables parallel download (5), colors (`Color` & `ILoveCandy`) and enables multi-lib
``` shell
# save previous config first; then copy the config there
sudo mv /etc/pacman.conf /etc/pacman.conf.old
sudo cp ~/.config/yay/pacman.conf /etc/pacman.conf
```

### Automatically install dependencies
If you are on arch run the installing helper files (careful: those will use sudo because they need root access, always check the content)
``` shell
~/.config/yay/get-yay.sh
~/.config/yay/yay-install.sh
~/.config/yay/extra-install.sh
```

### Change the shell to zsh (optional but recommended: change the root user shell too)
``` shell
# for current user
chsh -s $(which zsh)

# for root
sudo chsh -s $(which zsh)
```

### Enable SLiM
If this system will use copy the default configuration to `/etc/slim.conf`:
``` shell
sudo ln -s ~/.config/slim/slim.conf /etc/slim.conf
```

### Enable Stumpwm
If this system will use stumpwm create a xinit file starting stumpwm:
``` shell
# append to avoid destroyng the xinit if present
echo -e '#!/usr/bin/zsh \n\nexec ssh-agent /usr/bin/stumpwm' >> .xinitrc
```
This file is not included to avoid destroyng systems not using stumpw
You can uninstall it if you don't need it

### Systemd services
Enable the user services you need:
``` shell
systemctl enable --user emacs.service
systemctl enable --user libredefender-scan.timer
systemctl enable --user kanata.service
systemctl enable --user geoclue-agent.service
systemctl enable --user redshift.service
```

Enable the non-user services you need or may find useful:
``` shell
sudo systemctl enable cronie.service
sudo systemctl enable sysstat
sudo systemctl enable paccache.timer
sudo systemctl enable fstrim.timer
sudo systemctl enable reflector.timer
sudo systemctl enable firewalld.service
sudo systemctl enable nftables.service
sudo systemctl enable upower.service
sudo systemctl enable nordvpnd.service
# sddm is kept in case this installation uses kde plasma
# sudo systemctl enable sddm.service
```

### Btrfs
If using btrfs filesystem modify jobs to the send to the correct server.
Then link to them in cron:
``` shell
sudo ln -s .config/btrfs/cron.hourly/btrbk-snapshot /etc/cron.hourly/btrbk-snapshot
sudo ln -s .config/btrfs/cron.daly/btrbk /etc/cron.daily/btrbk
```

### Firefox
Install the really useful firefox add-ons in this list:
```https://addons.mozilla.org/en-US/firefox/collections/17876585/aless3/``` <br />
Also I HATE that pressing the ```alt``` key pops up the menu, you cat disable that behaviour going to ```about:config``` and setting ```ui.key.menuAccessKeyFocuses``` to ```false```
