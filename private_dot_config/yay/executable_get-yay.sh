#!/bin/env sh

# if executed as root or sudo, exit and do nothing
if [ $(whoami) = 'root' ]; then
	echo "ERROR: do NOT execute this script as root"
  exit 1
fi

cd ~
pacman -S --needed git base-devel
git clone https://aur.archlinux.org/yay.git
cd yay
makepkg -si
cd ~

echo "now running yay to sync mirrors etc, may need super user access"
yay
