#!/bin/env sh

# if executed as root or sudo, exit and do nothing
if [ $(whoami) = 'root' ]; then
	echo "ERROR: do NOT execute this script as root"
  exit 1
fi

echo "This script will install most things as the current user.
But some things cannot be installed without super user access.
This script will require super user access via sudo.\n
Always check the script you run when running something as super user: cat $(basename $0)"

# split approx 10 packages per call to avoid installing problems

# base things
yay -S base base-devel cmake dhcp dhcpcd iwd zstd openssh git usbutils inetutils sysstat

# grub things, uncomment if using grub
# yay -S grub grub-customizer

# good utilities
yay -S ripgrep fd yay zoxide bat bat-extras rsync openbsd-netcat git-delta gparted udiskie bottom net-tools

# Slim and stumpwm, uncomment if using stumpwm, keep commented out if using kde
# yay -S slim archlinux-themes-slim slim-sexy stumpwm stumpwm-contrib dunst wmname redshift alttab

# audio card things, avoiding pulse audio
yay -S pipewire pipewire-alsa pipewire-jack gst-plugin-pipewire libpulse pipewire-pulse wireplumber noisetorch-bin

# video and audio codecs and player
yay -S libdv wavpack mplayer

# X and X-like utils
yay -S evtest scrot xclip xorg arandr upower

# terminal emulator related stuff
yay -S alacritty zsh atuin zellij tmux

# WG & resolver needed
yay -S systemd-resolvconf wireguard-tools

# zip things
yay -S p7zip unrar tar

# user cli packages
yay -S kanata chezmoi downgrade speedtest-cli keychain tcpdump

# user gui packages
yay -S webcord firefox nordvpn-bin

# office deps and zathura, uncomment zathura if using it, the rest is still useful
yay -S unoconv calibre libreoffice-fresh
# yay -S zathura-pdf-poppler zathura zathura-ps zathura-cb zathura-djvu

# cron & basic useful cron jobs
yay -S iptables-nft
yay -S libredefender clamav cronie reflector firewalld

# niche things for other programs
yay -S entr rlwrap

# programming things
yay -S jdk-openjdk jdtls ccls pypy3 python python-pip npm nano aspell aspell-en aspell-it

# emacs building deps for version 28 not in the arch repo, just in case
yay -S imagemagick lib32-jansson libgccjit png++ tree-sitter ttf-hack tuntox libvterm

# install other emacs 29
yay -S emacs

# install last
yay -S informant
