#!/bin/env sh

# if executed as root or sudo, exit and do nothing
if [ $(whoami) = 'root' ]; then
	echo "ERROR: do NOT execute this script as root"
  exit 1
fi

echo "This script will install most things as the current user.
But some things cannot be installed without super user access.
This script will require super user access via sudo.\n
Always check the script you run when running something as super user: cat $(basename $0)"


printf "\n---\n\nfirst set up rust with rustup, this is needed to download bcrypt via pip"
rustup default stable && rustup toolchain install stable

printf "\n---\n\ninstall prettier, lang servers, etc as user"
npm install prettier @fsouza/prettierd prettier-plugin-java pyright npm-check-updates --save-dev

printf "\n---\n\ninstall pyright as super user because... Microsoft..."
sudo npm install -g pyright

printf "\n---\n\ninstall latest python with pyenv"
pyenv install 3

printf "\n---\n\nand set it as global under pyenv"
pyenv global $(pyenv latest 3)

printf "\n---\n\nthen install lsp-bridge python dependencies"
pip install epc orjson sexpdata six setuptools paramiko rapidfuzz

printf "\n---\n\nthen install zaread"
git clone https://github.com/aless3/zaread ~/.zaread
cd ~/.zaread
sudo make install

printf "\n---\n\nDone!"
