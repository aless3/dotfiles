#
# Example btrbk configuration file
#
#
# Please refer to the btrbk.conf(5) man-page for a complete
# description of all configuration options.
# For more examples, see README.md included with this package.
#
#   btrbk.conf(5): <https://digint.ch/btrbk/doc/btrbk.conf.5.html>
#   README.md:     <https://digint.ch/btrbk/doc/readme.html>
#
# Note that the options can be overridden per volume/subvolume/target
# in the corresponding sections.
#


# Enable transaction log
transaction_log            /var/log/btrbk.log

# Specify SSH private key for remote connections
ssh_identity               /home/aless/.ssh/id_ed25519
ssh_user                   pi

# Use sudo if btrbk or lsbtr is run by regular user
backend_local_user         btrfs-progs-sudo

# Enable stream buffer. Adding a buffer between the sending and
# receiving side is generally a good idea.
# NOTE: If enabled, make sure to install the "mbuffer" package!
stream_buffer              256m

# Directory in which the btrfs snapshots are created. Relative to
# <volume-directory> of the volume section.
# If not set, the snapshots are created in <volume-directory>.
#
# If you want to set a custom name for the snapshot (and backups),
# use the "snapshot_name" option within the subvolume section.
#
# NOTE: btrbk does not automatically create this directory, and the
# snapshot creation will fail if it is not present.
#
snapshot_dir               .snapshots

# Always create snapshots. Set this to "ondemand" to only create
# snapshots if the target volume is reachable. Set this to "no" if
# snapshot creation is done by another instance of btrbk.
snapshot_create            ondemand

# Perform incremental backups (set to "strict" if you want to prevent
# creation of non-incremental backups if no parent is found).
incremental                yes

# Specify after what time (in full hours after midnight) backups/
# snapshots are considered as a daily backup/snapshot
preserve_hour_of_day       0

# Specify on which day of week weekly/monthly backups are to be
# preserved.
preserve_day_of_week       monday

# Preserve all snapshots for a minimum period of time.
snapshot_preserve_min      latest

# Retention policy for the source snapshots.
snapshot_preserve          12h 7d

# Preserve all backup targets for a minimum period of time.
target_preserve_min        latest

# Retention policy for backup targets:
target_preserve            28d 12w 4m 1y

# Retention policy for archives ("btrbk archive" command):
archive_preserve_min       latest
archive_preserve           0h 1d 1w 1m 1y

# Enable compression for remote btrfs send/receive operations:
stream_compress            zstd
stream_compress_level      default #
stream_compress_threads    default

# Enable lock file support: Ensures that only one instance of btrbk
# can be run at a time.
lockfile                   /var/lock/btrbk.lock

# Don't wait for transaction commit on deletion. Enable this to make
# sure the deletion of subvolumes is committed to disk when btrbk
# terminates.
#btrfs_commit_delete no

timestamp_format           long
backend_remote             btrfs-progs-sudo


volume /
  target ssh://192.168.1.200/home/pi/disks/hdd2/backups
    subvolume .


#
# Volume section (optional): "volume <volume-directory>"
#
#   <volume-directory>  Base path within a btrfs filesystem
#                       containing the subvolumes to be backuped
#                       (usually the mount-point of a btrfs filesystem
#                       mounted with subvolid=5 option).
#
# Subvolume section: "subvolume <subvolume-name>"
#
#   <subvolume-name>    Subvolume to be backuped, relative to
#                       <volume-directory> in volume section.
#
# Target section: "target <type> <volume-directory>"
#
#   <type>              (optional) type, defaults to "send-receive".
#   <volume-directory>  Directory within a btrfs filesystem
#                       receiving the backups.
#
# NOTE: The parser does not care about indentation, this is only for
# human readability. All options apply to the last section
# encountered, overriding the corresponding option of the upper
# section. This means that the global options must be set on top,
# before any "volume", "subvolume" or "target section.
#


# snapshot_dir /btrbk_snapshots
# target       /mnt/btr_backup
# subvolume    /
# subvolume    /home
