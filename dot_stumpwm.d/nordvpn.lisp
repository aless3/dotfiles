;; Nordvpn map setup

;; status
(defcommand nordvpn-status () ()
            "message status of connection to nordvpn"
            (run-shell-command "nord=\"$(nordvpn status)\" && notify-send 'NordVPN status' \"${nord:11}\""))

;; connect
(defcommand nordvpn-connect () ()
            "quick connect to nordvpn"
            (run-shell-command "nordvpn connect"))

;; disconnect
(defcommand nordvpn-disconnect () ()
            "disconnect from nordvpn"
            (run-shell-command "nordvpn disconnect"))

;; toggle
(defcommand nordvpn-toggle () ()
            "quick connect to nordvpn if disconnected, connect otherwise"
            (run-shell-command "if echo $(nordvpn status) | rg 'Disc'; then nordvpn connect; else nordvpn disconnect; fi"))

(defparameter *nordvpn-map*
              (let ((m (make-sparse-keymap)))
                (define-key m (kbd "n") "nordvpn-status")
                (define-key m (kbd "s") "nordvpn-status")
                (define-key m (kbd "b") "nordvpn-toggle")
                (define-key m (kbd "t") "nordvpn-toggle")
                (define-key m (kbd "c") "nordvpn-connect")
                (define-key m (kbd "r") "nordvpn-connect")
                (define-key m (kbd "d") "nordvpn-disconnect")
                (define-key m (kbd "h") "nordvpn-disconnect")
                m))
