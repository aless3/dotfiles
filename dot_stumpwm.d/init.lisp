(setq *startup-message* (format nil "Welcome back Ale <3"))

(when *initializing* ;; load ql dependencies only once!
  (load "~/.stumpwm.d/quicklisp.lisp"))

(load-module "stumptray")
(load-module "battery-portable")
(load-module "cpu")
(load-module "mem")
(load-module "notifications")

(load "~/.stumpwm.d/visual.lisp")
(load "~/.stumpwm.d/history-managers.lisp")
(load "~/.stumpwm.d/keybindings.lisp")

;; Load autostart programs but only once at startup
(when *initializing*
  (load "~/.stumpwm.d/autostart.lisp")
  (stumptray:stumptray))
