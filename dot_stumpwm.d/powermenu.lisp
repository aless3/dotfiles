;;; Power menu map

;; Shutdown
(defcommand shutdown (confirm) ((:y-or-n "Confirm Shutdown "))
            "Ask for the user to confirm before shutting down."
	          (if confirm
		            (run-shell-command "poweroff")))

;; Reboot
(defcommand reboot (confirm) ((:y-or-n "Confirm Reboot "))
            "Ask for the user to confirm before rebooting."
	          (if confirm
		            (run-shell-command "reboot")))

(defparameter *power-map*
              (let ((m (make-sparse-keymap)))
                (define-key m (kbd "u") "shutdown")
                (define-key m (kbd "h") "reboot")
                m))
