;; Search the internet opening firefox automagically

(load-module "searchengines")

(setf searchengines:*search-browser-executable* "firefox")
(setf searchengines:*search-browser-params* '("--new-tab"))

(defparameter *URL-DDG* "https://duckduckgo.com/?q=~a")

(searchengines:make-searchengine-prompt "search-prompt" "DuckDuckGo" *URL-DDG* "DuckDuckGo search")

(searchengines:make-searchengine-selection "search-selection" *URL-DDG* "DuckDuckGo search")

(searchengines:make-searchengine-augmented "search-augmented" "Augmented DuckDuckGo" *URL-DDG* "DuckDuckGo search")

(defparameter *search-map*
  (let ((m (make-sparse-keymap)))
    (define-key m (kbd "s") "search-prompt")
    (define-key m (kbd "C-s") "search-prompt")
    (define-key m (kbd "S") "search-selection")
    (define-key m (kbd "C-S") "search-selection")
    (define-key m (kbd "C-s-s") "search-augmented")
    (define-key m (kbd "C-s-S") "search-augmented")
    m))
