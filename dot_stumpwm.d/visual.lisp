(in-package :stumpwm)

;; Font - name generated with xfontsel
(set-font "-misc-hack-bold-r-normal-*-15-*-*-*-*-*-*-*")

;; Theme
(load "~/.stumpwm.d/theme.lisp")

;; Modeline config
(load "~/.stumpwm.d/modeline.lisp")

;; Set the mouse focus policy to following
;; the mouse on hover (not only on click)
(setf *mouse-focus-policy* :sloppy)

;; Set where the new window appear
(setf *new-window-preferred-frame* '(:empty :unfocused :last :focused))


;;; When windows are desroyed window numbers are not synced
;;; 2kays <https://github.com/2kays> posted a solution on
;;; the TipsAndTricks section of the wiki
;;; This will repack window numbers every time a window is killed
(stumpwm:add-hook stumpwm:*destroy-window-hook*
                  #'(lambda (win) (stumpwm:repack-window-numbers)))
