(in-package :stumpwm)

;; Double check prefix-key is set correctly
(set-prefix-key (kbd "C-SPC"))

(define-key *top-map* (kbd "M-TAB") "exec alttab")
;; (define-key *top-map* (kbd "M-ISO_Left_Tab") "pull-hidden-other")

;; Run alacritty
(define-key *root-map* (kbd "u") "exec alacritty")
(define-key *root-map* (kbd "C-u") "exec alacritty")

;; Run emacs or emacsclient
(define-key *root-map* (kbd "e") "exec emacsclient -c -n -a ''")
(define-key *root-map* (kbd "C-e") "exec emacs")

;; Run firefox
(define-key *root-map* (kbd "t") "exec firefox")
(define-key *root-map* (kbd "C-t") "exec firefox")

;; Run firefox
(define-key *root-map* (kbd "d") "exec vesktop")
(define-key *root-map* (kbd "C-d") "exec vesktop")

;; Emacs Style Frame Splitting
(define-key *root-map* (kbd "1") "only")
(define-key *root-map* (kbd "2") "vsplit")
(define-key *root-map* (kbd "3") "hsplit")

;; Screenshot command
(defcommand screenshot () ()
            "Do we wanna Scrot? Yeah! We wanna Scrot!"
            (run-shell-command "cd /home/aless/screenshots/; scrot"))

(define-key *root-map* (kbd "S") "screenshot")

;;; Load other binding definitions and files

;; search map
(load "~/.stumpwm.d/searchengines.lisp")
(define-key *root-map* (kbd "s") *search-map*)
(define-key *root-map* (kbd "C-s") *search-map*)

;; power menu map
(load "~/.stumpwm.d/powermenu.lisp")
(define-key *root-map* (kbd "h") *power-map*)

;; nord vpn map
(load "~/.stumpwm.d/nordvpn.lisp")
(define-key *root-map* (kbd "b") *nordvpn-map*)
