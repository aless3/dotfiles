(in-package :stumpwm)

(setf *mode-line-timeout* 1)
(setf *window-format* "%n %9c: %11t")
(setf *time-modeline-string* "%T")

(defun get-signal-strength ()
  (string-trim
   (concat "." (string #\newline))
   (run-shell-command "awk 'NR==3{print $3}' /proc/net/wireless" t)))

;; Set model-line format
(setf *screen-mode-line-format*
      (list
       "%W" ;; %v
       "^>"
       "^[^6>> ^]"
       "WiFi[" '(:eval (get-signal-strength)) "]"
       "^[^6 ||| ^]"

       ;; "[" "%W" "]"
       ;; "^[^6 ||| ^]"

       "BAT[%B]"
       "^[^6 ||| ^]"
       "[%d]"
       "%T" ;; let some space for the stump tray
       ))

;; Turn on the modeline - toggle to update some things that are not updated with enable
(mapcar (lambda (head)
          (toggle-mode-line (current-screen) head))
        (screen-heads (current-screen)))

;; Turn on the modeline - to be sure it is enabled after toggle
(mapcar (lambda (head)
          (enable-mode-line (current-screen) head t))
        (screen-heads (current-screen)))
