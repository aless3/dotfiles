;;; History managers for:
;;; - Clipboard (clipboard-history)
;;; - StumpWM Commands (command-history)
;;; - StumpWM shell commands (shell-command-history)

;; Clipboard
(load-module "clipboard-history")
(define-key *root-map* (kbd "C-v") "show-clipboard-history")
(clipboard-history:start-clipboard-manager) ; start the polling timer process

;; ;; StumpWM commands
;; (load-module "command-history")

;; ;; StumpWM shell commands
;; (setf shell-command-history:*shell-command-history-file*
;;       "~/.cache/stumpwm/shell-command-history") ; change the default file
;; (load-module "shell-command-history")
