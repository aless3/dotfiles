;; JAVA has some issues with non reparenting window managers
;; While we could set _JAVA_AWT_WM_NONREPARENTING=1 lets just say we are oracles tiling window manager
;; I find that funnier
(run-shell-command "wmname LG3D")

;; Open alacritty at startup - it's always useful to have a terminal
(run-shell-command "alacritty")

;; Notifications
(run-shell-command "/usr/bin/dunst")

;; Disk mounting
(run-shell-command "/usr/bin/udiskie --no-automount")

;; Alt tab window switching
(run-shell-command "alttab")

;; Launch slynk
(slynk:create-server :port 4004
                     :dont-close t)
