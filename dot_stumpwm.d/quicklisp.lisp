;;; Quicklisp setup
;; all code is commented - it should not be needed or needed only once per installation

;; load quicklisp, should already be installed
;; see https://www.quicklisp.org/beta/#installation
(let ((quicklisp-init (merge-pathnames "quicklisp/setup.lisp"
                                       (user-homedir-pathname))))
  (when (probe-file quicklisp-init)
    (load quicklisp-init)))

;; use quicklisp to load those libraries
(ql:quickload "clx")
(ql:quickload "cl-ppcre")
(ql:quickload "alexandria")
(ql:quickload "drakma")
(ql:quickload "xembed")
(ql:quickload :slynk)

;; this library I am not sure is used, left to be safe
;; (ql:quickload "truetype-clx")
